using System.Collections.Generic;
using System.Linq;

namespace BankOCR
{
    public class Digit : IDigit
    {
        private readonly string inputdata;

        private const string ZERO = " _ " +
                                    "| |" +
                                    "|_|";

        private const string ONE = "   " +
                                   "  |" +
                                   "  |";

        private const string TWO = " _ " +
                                   " _|" +
                                   "|_ ";

        private const string THREE = " _ " +
                                     " _|" +
                                     " _|";

        private const string FOUR = "   " +
                                    "|_|" +
                                    "  |";

        private const string FIVE = " _ " +
                                    "|_ " +
                                    " _|";

        private const string SIX = " _ " +
                                   "|_ " +
                                   "|_|";

        private const string SEVEN = " _ " +
                                     "  |" +
                                     "  |";

        private const string EIGHT = " _ " +
                                     "|_|" +
                                     "|_|";

        private const string NINE = " _ " +
                                    "|_|" +
                                    " _|";

        static readonly Dictionary<string, int> DIGITS_COLLECTION = new Dictionary<string, int>();

        static Digit()
        {
            DIGITS_COLLECTION.Add(ZERO, 0);
            DIGITS_COLLECTION.Add(ONE, 1);
            DIGITS_COLLECTION.Add(TWO, 2);
            DIGITS_COLLECTION.Add(THREE, 3);
            DIGITS_COLLECTION.Add(FOUR, 4);
            DIGITS_COLLECTION.Add(FIVE, 5);
            DIGITS_COLLECTION.Add(SIX, 6);
            DIGITS_COLLECTION.Add(SEVEN, 7);
            DIGITS_COLLECTION.Add(EIGHT, 8);
            DIGITS_COLLECTION.Add(NINE, 9);
        }

        public Digit(string inputdata)
        {
            this.inputdata = inputdata;
        }

        public int DigitValue
        {
            get
            {
                return DIGITS_COLLECTION.Where(d => this.inputdata.CompareTo(d.Key) == 0).First().Value;

            }
        }
    }
}