using System;

namespace BankOCR
{
    public interface IDigitFactory
    {
        IDigit CreateDigit(string inputdata);
        IDigit CreateDigit(string[] inputdata);
    }
}