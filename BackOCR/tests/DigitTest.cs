﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BankOCR
{
    [TestFixture]
    class DigitTest
    {
        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Zero_Should_Return_Int_Value_Of_Zero()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*   _ 
             *  | |
             *  |_|
             */
            string[] input = new string[] { " _ ", "| |", "|_|" };
            int expected = 0;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));

        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_One_Should_Return_Int_Value_Of_One()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*    
             *   |
             *   |
             */
            string[] input = new string[] { "   ", "  |", "  |" };
            int expected = 1;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));

        }


        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Two_Should_Return_Int_Value_Of_Two()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*    _ 
              *   _|
              *  |_
              */
            string[] input = new string[] { " _ ", " _|", "|_ " };
            int expected = 2;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Three_Should_Return_Int_Value_Of_Three()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*    _ 
              *   _|
              *   _|
              */
            string[] input = new string[] { " _ ", " _|", " _|" };
            int expected = 3;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Four_Should_Return_Int_Value_Of_Four()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*      
              *  |_|
              *    |
              */
            string[] input = new string[] { "   ", "|_|", "  |" };
            int expected = 4;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Five_Should_Return_Int_Value_Of_Five()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*    _ 
              *  |_ 
              *   _|
              */
            string[] input = new string[] { " _ ", "|_ ", " _|" };
            int expected = 5;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Six_Should_Return_Int_Value_Of_Six()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*    _ 
              *  |_ 
              *  |_|
              */
            string[] input = new string[] { " _ ", "|_ ", "|_|" };
            int expected = 6;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Seven_Should_Return_Int_Value_Of_Seven()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*    _ 
              *    |
              *    |
              */
            string[] input = new string[] { " _ ", "  |", "  |" };
            int expected = 7;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Eight_Should_Return_Int_Value_Of_Eight()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*   _ 
             *  |_|
             *  |_|
             */
            string[] input = new string[] { " _ ", "|_|", "|_|" };
            int expected = 8;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));

        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Forms_Nine_Should_Return_Int_Value_Of_Nine()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*   _ 
             *  |_|
             *   _|
             */
            string[] input = new string[] { " _ ", "|_|", " _|" };
            int expected = 9;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            result = digit.DigitValue;

            // assert

            Assert.That(result, Is.EqualTo(expected));

        }

        [Test]
        public void Digit_Construct_With_Array_Of_Chars_That_Do_NOT_Form_Any_Of_Digits_Should_Throw_InvalidOperationException_With_Message_Sequence_Contains_No_Elements()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            /*     
             *  |_|
             *   _|
             */
            string[] input = new string[] { "   ", "|_|", " _|" };
            int expected = 9;
            int result = -1;

            //act
            var digit = idf.CreateDigit(input);
            try
            {
                result = digit.DigitValue;
            }
            catch (InvalidOperationException exception)
            {
                Assert.That(exception.Message, Is.EqualTo("Sequence contains no elements"));
            }

        }

    }

}