﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BankOCR
{
    [TestFixture]
    class DigitFactoryTest
    {
        [Test]
        public void Factory_Should_Be_Able_To_Build_Digit()
        {
            // arrange
            IDigitFactory idf = new DigitFactory();

            //act
            var r = idf.CreateDigit(new string[] { "a", "a" });

            //assert
            Assert.That(r, Is.Not.Null);
        }

    }
}