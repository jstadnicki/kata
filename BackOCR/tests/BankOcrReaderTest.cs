﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BankOCR
{
    [TestFixture]
    class BankOcrReaderTest
    {
        [Test]
        public void Given_BankOcrReader_When_Input_Contatins_9Zeros_Should_Return_String_Filled_With_9Zeros()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n| || || || || || || || || |\n|_||_||_||_||_||_||_||_||_|";
            /*  _  _  _  _  _  _  _  _  _ 
             * | || || || || || || || || |
             * |_||_||_||_||_||_||_||_||_|
             */

            string expected = "000000000";
            string result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.Digits;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Input_Contatins_9Ones_Should_Return_String_Filled_With_9Ones()
        {
            //arrange
            string input = "                           \n  |  |  |  |  |  |  |  |  |\n  |  |  |  |  |  |  |  |  |";
            /*
            *                           
            *  |  |  |  |  |  |  |  |  |
            *  |  |  |  |  |  |  |  |  |
            */
            string expected = "111111111";
            string result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.Digits;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Created_With_Text_To_Parse_Should_Split_Input_String_Into_Array_Of_Three_Elems()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n| || || || || || || || || |\n|_||_||_||_||_||_||_||_||_|";
            /*  _  _  _  _  _  _  _  _  _ 
             * | || || || || || || || || |
             * |_||_||_||_||_||_||_||_||_|
             */

            int expected = 3;
            int result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, null);
            result = bor.LineCount;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Created_With_Text_To_Parse_Should_Contain_Nine_Digits_Parsed_From_Input()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n| || || || || || || || || |\n|_||_||_||_||_||_||_||_||_|";
            /*  _  _  _  _  _  _  _  _  _ 
             * | || || || || || || || || |
             * |_||_||_||_||_||_||_||_||_|
             */

            int expected = 9;
            int result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.DigitsCount;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Created_With_Text_Of_Zeros_To_Parse_Should_Be_Able_To_Return_String_Buffer_For_Each_Digit()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n| || || || || || || || || |\n|_||_||_||_||_||_||_||_||_|";
            /*  _  _  _  _  _  _  _  _  _ 
             * | || || || || || || || || |
             * |_||_||_||_||_||_||_||_||_|
             */

            string expected = " _ | ||_|";
            string result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, null);
            result = bor.GetDigitsRawDataAt(0);

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Created_With_Text_Of_Ones_To_Parse_Should_Be_Able_To_Return_String_Buffer_For_Each_Digit()
        {
            //arrange
            string input = "                           \n  |  |  |  |  |  |  |  |  |\n  |  |  |  |  |  |  |  |  |";
            /*                            
             *   |  |  |  |  |  |  |  |  |
             *   |  |  |  |  |  |  |  |  |
             */

            string expected = "     |  |";
            string result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, null);
            result = bor.GetDigitsRawDataAt(0);

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Input_Contatins_9Twos_Should_Return_String_Filled_With_9Twos()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n _| _| _| _| _| _| _| _| _|\n|_ |_ |_ |_ |_ |_ |_ |_ |_ ";
            /*
             _  _  _  _  _  _  _  _  _ 
             _| _| _| _| _| _| _| _| _|
            |_ |_ |_ |_ |_ |_ |_ |_ |_ 
            */
            string expected = "222222222";
            string result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.Digits;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Input_Contatins_123456789_Encoded_In_Ascii_Should_Return_String_Filled_With_123456789()
        {
            //arrange
            string input = "    _  _     _  _  _  _  _ \n  | _| _||_||_ |_   ||_||_|\n  ||_  _|  | _||_|  ||_| _|";
            /*  _  _     _  _  _  _  _
              | _| _||_||_ |_   ||_||_|
              ||_  _|  | _||_|  ||_| _|
            */
            string expected = "123456789";
            string result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.Digits;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }


        [Test]
        public void Given_BankOcrReader_When_Account_Number_Is_Invalid_Line_123456789_Should_Detect_It_As_Invalid()
        {
            //arrange
            string input = "    _  _     _  _  _  _  _ \n  | _| _||_||_ |_   ||_||_|\n  ||_  _|  | _||_|  ||_| _|";
            /*  _  _     _  _  _  _  _
              | _| _||_||_ |_   ||_||_|
              ||_  _|  | _||_|  ||_| _|
            */
            bool expected = false;
            bool result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.IsValid;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Account_Number_Is_Valid_Like_000000000_Should_Detect_It_As_Valid()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n| || || || || || || || || |\n|_||_||_||_||_||_||_||_||_|";
            /*  _  _  _  _  _  _  _  _  _ 
             * | || || || || || || || || |
             * |_||_||_||_||_||_||_||_||_|
             */
            bool expected = true;
            bool result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.IsValid;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Account_Number_Is_Valid_Like_705882866_Should_Detect_It_As_Valid()
        {
            //arrange
            string input = " _  _  _  _  _  _  _  _  _ \n  || ||_ |_||_| _||_||_ |_ \n  ||_| _||_||_||_ |_||_||_|";
            // _  _  _  _  _  _  _  _  _ 
            //  || ||_ |_||_| _||_||_ |_ 
            //  ||_| _||_||_||_ |_||_||_|
            bool expected = true;
            bool result;

            //act

            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.IsValid;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Given_BankOcrReader_When_Input_String_Is_Invalid_Should_Insert_Question_Mark_Instead_Of_Invalid_Digit()
        {
            //arrange
            string input = " _     _  _  _  _  _  _  _ \n  || ||_ |_||_| _||_||_ |_ \n  ||_| _||_||_||_ |_||_||_|";
            // _     _  _  _  _  _  _  _ 
            //  || ||_ |_||_| _||_||_ |_ 
            //  ||_| _||_||_||_ |_||_||_|
            string expected = "7?5882866";
            string result;

            //act
            IBankOcrReader bor = new BankOcrReader(input, new DigitFactory());
            result = bor.Digits;

            //assert
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
