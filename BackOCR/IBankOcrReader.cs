namespace BankOCR
{
    public interface IBankOcrReader
    {
        bool IsValid { get; }

        string GetDigitsRawDataAt(int index);

        int DigitsCount { get; }

        int LineCount { get; }

        string Digits { get; }
    }
}