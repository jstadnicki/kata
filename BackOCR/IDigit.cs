namespace BankOCR
{
    public interface IDigit
    {
        int DigitValue { get; }
    }
}