using System;
using System.Text;

namespace BankOCR
{
    public class DigitFactory : IDigitFactory
    {
        public IDigit CreateDigit(string inputdata)
        {
            return BuildDigit(inputdata);
        }


        public IDigit CreateDigit(string[] inputdata)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in inputdata)
            {
                sb.Append(s);
            }

            return this.BuildDigit(sb.ToString());

        }

        private IDigit BuildDigit(string inputData)
        {
            return new Digit(inputData);
        }
    }
}