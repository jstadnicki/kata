using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankOCR
{
    public class BankOcrReader : IBankOcrReader
    {
        string[] inputdata = new string[3];
        List<string> rawData;

        private IDigitFactory digitFactory;

        private List<IDigit> digits;

        public BankOcrReader(string input, DigitFactory digitFactory)
        {
            this.digitFactory = digitFactory;
            System.Diagnostics.Debug.Assert(input.Length == 83);
            inputdata = input.Split(new char[] { '\n' });
        }

        public bool IsValid
        {
            get
            {
                if (digits == null)
                {
                    this.CreateDigits();
                }

                int sum = 0;
                for (int i = 1; i <= this.digits.Count(); i++)
                {
                    var d = this.digits[i - 1].DigitValue;
                    var a = d * i;
                    sum += a;

                }
                return sum % 11 == 0;
            }
        }

        private string CreateStringFromDigits()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var d in this.digits)
            {
                try
                {
                    sb.Append(d.DigitValue.ToString());
                }
                catch (InvalidOperationException /*exception*/)
                {
                    sb.Append("?");
                }
            }
            return sb.ToString();
        }

        private void CreateDigits()
        {
            this.digits = new List<IDigit>();
            for (int i = 0; i < this.inputdata[0].Length / 3; i++)
            {
                digits.Add(this.digitFactory.CreateDigit(this.GetDigitsRawDataAt(i)));
            }
        }

        public string GetDigitsRawDataAt(int index)
        {
            if (this.rawData == null)
            {
                this.CreateRawDataList();
            }
            return this.rawData[index];
        }

        private void CreateRawDataList()
        {
            this.rawData = new List<string>(this.inputdata[0].Length / 3);

            for (int i = 0; i < this.inputdata[0].Length / 3; ++i)
            {
                this.rawData.Add(
                    this.inputdata[0].Substring(i * 3, 3) +
                    this.inputdata[1].Substring(i * 3, 3) +
                    this.inputdata[2].Substring(i * 3, 3));
            }
        }

        public int DigitsCount
        {
            get
            {
                if (this.digits == null)
                {
                    CreateDigits();
                }
                return this.digits.Count();
            }
        }

        public string Digits
        {
            get
            {
                if (this.digits == null)
                {
                    CreateDigits();
                }

                return CreateStringFromDigits();

            }
        }

        public int LineCount
        {
            get
            {
                return this.inputdata.Length;
            }
        }
    }
}